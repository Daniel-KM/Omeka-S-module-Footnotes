<?php declare(strict_types=1);

/*
 * Copyright 2021 Daniel Berthereau
 *
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software’s author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user’s attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software’s suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

namespace Footnotes;

if (!class_exists(\Generic\AbstractModule::class)) {
    require file_exists(dirname(__DIR__) . '/Generic/AbstractModule.php')
        ? dirname(__DIR__) . '/Generic/AbstractModule.php'
        : __DIR__ . '/src/Generic/AbstractModule.php';
}

use Generic\AbstractModule;
use Laminas\EventManager\Event;
use Laminas\EventManager\SharedEventManagerInterface;
use Omeka\Module\Exception\ModuleCannotInstallException;

class Module extends AbstractModule
{
    const NAMESPACE = __NAMESPACE__;

    protected function preInstall(): void
    {
        $js = __DIR__ . '/asset/vendor/littlefoot/littlefoot.js';
        if (!file_exists($js)) {
            $services = $this->getServiceLocator();
            $t = $services->get('MvcTranslator');
            throw new ModuleCannotInstallException(
                sprintf(
                    $t->translate('The library "%s" should be installed.'), // @translate
                    'LittleFoot.js'
                ) . ' '
                . $t->translate('See module’s installation documentation.')); // @translate
        }
    }

    public function attachListeners(SharedEventManagerInterface $sharedEventManager): void
    {
        $sharedEventManager->attach(
            '*',
            'view.layout',
            [$this, 'addHeaders']
        );
    }

    public function addHeaders(Event $event): void
    {
        /** @var \Omeka\Mvc\Status $status */
        $services = $this->getServiceLocator();
        $status = $services->get('Omeka\Status');

        if ($status->isSiteRequest()) {
            $this->displayFootnotes($event);
            return;
        }

        // Skip other requests (api and not-site public part).
        if (!$status->isAdminRequest()) {
            return;
        }

        // TODO Add more checks to avoid to add the display js.
        $routeMatch = $status->getRouteMatch();
        $action = $routeMatch->getParam('action');
        if ($action === 'browse') {
            return;
        }

        if ($action !== 'edit') {
            $this->displayFootnotes($event);
            return;
        }

        $view = $event->getTarget();
        $assetUrl = $view->plugin('assetUrl');
        $view->headLink()
            ->appendStylesheet($assetUrl('vendor/littlefoot/littlefoot.css', 'Footnotes'));
        $view->headScript()
            ->appendFile($assetUrl('vendor/littlefoot/littlefoot.js', 'Footnotes'), 'text/javascript', ['defer' => 'defer'])
            ->appendFile($assetUrl('js/footnotes-admin.js', 'Footnotes'), 'text/javascript', ['defer' => 'defer']);
    }

    /**
     * Add headers to display footnotes (public side and admin non-edit views).
     *
     * The footnotes can be used in media html or when the data type "html" is
     * used, so nearly everywhere.
     */
    public function displayFootnotes(Event $event): void
    {
        $view = $event->getTarget();
        $assetUrl = $view->plugin('assetUrl');
        $view->headLink()
            ->appendStylesheet($assetUrl('vendor/littlefoot/littlefoot.css', 'Footnotes'));
        $view->headScript()
            ->appendFile($assetUrl('vendor/littlefoot/littlefoot.js', 'Footnotes'), 'text/javascript', ['defer' => 'defer'])
            ->appendFile($assetUrl('js/footnotes.js', 'Footnotes'), 'text/javascript', ['defer' => 'defer']);
    }
}
