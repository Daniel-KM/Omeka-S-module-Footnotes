Footnotes (module for Omeka S)
==============================

> __New versions of this module and support for Omeka S version 3.0 and above
> are available on [GitLab], which seems to respect users and privacy better
> than the previous repository.__

[Footnotes] is a module for [Omeka S] that makes footnotes comfortable for
users: they can read them in a popup instead of in the bottom of the page.

***Important***: The footnote editor is manual for now: the user should edit the
html source to add notes. If you want a simpler user experience, use the module
[Block Plus], that includes a graphical editor of footnotes.

This module integrates the library [littlefoot.js]. It has the same features
than the Omeka Classic plugin [Omeka Footnotes JS].


Installation
------------

The module uses an external library [littlefoot.js], so use the release zip to
install it, or use and init the source.

* From the zip

Download the last release [Footnotes.zip] from the list of releases (the master
does not contain the dependency), and uncompress it in the `modules` directory.

* From the source and for development:

If the module was installed from the source, rename the name of the folder of
the module to `Footnotes`, and go to the root module, and run:

```sh
composer install --no-dev
```

Then install it like any other Omeka module.


Usage
-----

First, you need to skip html purifying in the main settings of Omeka.

***Important***: the edit part is not integrated. See below for the current
usage, that requires to add note in the html source.

Some buttons are automatically added in the Omeka editor in order to add, remove
and reorder footnotes. Simply click on them and you will see the footnotes in
the bottom of the block in the public side.

### Configuration

In the Omeka main config, you may remove html purifier (not recommended for
security), or add `*.id` in the list of allowed HTML attributes.

### To add/edit footnotes

a. Position your cursor in the place you'd like to add a footnote, then press
  the "Add Footnote" button on the toolbar. A footnote link will be inserted at
  that point, and a footnote text snippet will appear at the end of the text
  block.

b. To change the content of the footnote, edit the text snippet at the bottom of
  the editor that corresponds to your numbered footnote. If citing a source,
  best practice is to do so in a standard bibliographic format for your
  discipline and apply a link if it can be accessed online.

c. Refer to (3) to update the order of your footnotes if necessary (in most
  cases, this should be handled automatically).

### To remove a footnote

a. Highlight the footnote link in the text. You may highlight multiple footnotes
  at the same time, but note that all footnotes you highlight will be deleted.

b. Press the "Delete Selected Footnotes" button on the toolbar. The footnotes
  you selected, along with their associated text snippets, will be removed.

c. Refer to (3) to update the order of your footnotes if necessary (in most
  cases, this should be handled automatically).

### To reorder footnotes:

a. Press the "Update Footnotes" button on the toolbar. This will reorder your
  footnotes in order of appearance.


Usage from the html source
--------------------------

If you have an advanced usage, you can add footnotes manually in the html source:

```html
<!-- Links -->
<p>
  This text has a reference here
  <sup id="fnref:1">
    <a href="#fn:1">1</a>
  </sup>
  etc.
</p>

<!-- Footnote List -->
<div class="footnotes">
  <ol>
    <li class="footnote" id="fn:1">
      <p>This is a footnote that is displayed in a popup in pages. <a href="#fnref:1" title="return to article"> ↩</a></p>
      <p></p>
    </li>
  </ol>
</div>
```


Warning
-------

Use it at your own risk.

It’s always recommended to backup your files and your databases and to check
your archives regularly so you can roll back if needed.


Troubleshooting
---------------

See online issues on the [module issues] page on GitLab.


License
-------

### Module

This module is published under the [CeCILL v2.1] license, compatible with
[GNU/GPL] and approved by [FSF] and [OSI].

This software is governed by the CeCILL license under French law and abiding by
the rules of distribution of free software. You can use, modify and/ or
redistribute the software under the terms of the CeCILL license as circulated by
CEA, CNRS and INRIA at the following URL "http://www.cecill.info".

As a counterpart to the access to the source code and rights to copy, modify and
redistribute granted by the license, users are provided only with a limited
warranty and the software’s author, the holder of the economic rights, and the
successive licensors have only limited liability.

In this respect, the user’s attention is drawn to the risks associated with
loading, using, modifying and/or developing or reproducing the software by the
user in light of its specific status of free software, that may mean that it is
complicated to manipulate, and that also therefore means that it is reserved for
developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software’s suitability as
regards their requirements in conditions enabling the security of their systems
and/or data to be ensured and, more generally, to use and operate it in the same
conditions as regards security.

The fact that you are presently reading this means that you have had knowledge
of the CeCILL license and that you accept its terms.

### Library LittleFoot

License MIT, see the library.


Copyright
---------

### Library

See the library [littlefoot.js].

### Module

* Copyright [Chris Padilla], 2020
* Copyright Daniel Berthereau, 2021 (see [Daniel-KM] on GitLab)


[Footnotes]: https://gitlab.com/Daniel-KM/Omeka-S-module-Footnotes
[Omeka S]: https://omeka.org/s
[littlefoot.js]: https://littlefoot.js.org/
[Omeka Footnotes JS]: https://omeka.org/classic/plugins/OmekaFootnotesJS
[installing a module]: https://dev.omeka.org/docs/s/user-manual/modules/#installing-modules
[Footnotes.zip]: https://gitlab.com/Daniel-KM/Omeka-S-module-Footnotes/-/releases
[module issues]: https://gitlab.com/Daniel-KM/Omeka-S-module-Footnotes/-/issues
[CeCILL v2.1]: https://www.cecill.info/licences/Licence_CeCILL_V2.1-en.html
[GNU/GPL]: https://www.gnu.org/licenses/gpl-3.0.html
[FSF]: https://www.fsf.org
[OSI]: https://opensource.org
[MIT]: https://opensource.org
[GitLab]: https://gitlab.com/Daniel-KM
[Chris Padilla]: https://github.com/padillac
[GitLab]: https://gitlab.com/Daniel-KM
[Daniel-KM]: https://gitlab.com/Daniel-KM "Daniel Berthereau"
