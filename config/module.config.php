<?php declare(strict_types=1);

namespace Footnotes;

return [
    'view_helpers' => [
        'invokables' => [
            'ckEditor' => View\Helper\CkEditor::class,
        ],
    ],
    'js_translate_strings' => [
    ],
];
